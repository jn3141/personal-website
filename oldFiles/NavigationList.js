import React from 'react';
import { Link } from 'react-router-dom'
import { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import AccountCircle from 'material-ui-icons/AccountCircle'
import Build from 'material-ui-icons/Build'
import Create from 'material-ui-icons/Create';
import School from 'material-ui-icons/School'
import StarIcon from 'material-ui-icons/Star';
import Work from 'material-ui-icons/Work';

export const NavigationList = (
    <div>
        <ListItem button component={Link} to='/'>
            <ListItemIcon>
                <AccountCircle />
            </ListItemIcon>
            <ListItemText primary="Introduction" />
        </ListItem>
        <ListItem button component={Link} to='/work-experience'>
            <ListItemIcon>
                <Work />
            </ListItemIcon>
            <ListItemText primary="Work Experience" />
        </ListItem>
        <ListItem button component={Link} to='/education'>
            <ListItemIcon>
                <School />
            </ListItemIcon>
            <ListItemText primary="Education" />
        </ListItem>
        <ListItem button component={Link} to='/projects'>
            <ListItemIcon>
                <StarIcon />
            </ListItemIcon>
            <ListItemText primary="Projects" />
        </ListItem>
        <ListItem button component={Link} to='/extracurricular'>
            <ListItemIcon>
                <Build />
            </ListItemIcon>
            <ListItemText primary="Extracurricular" />
        </ListItem>
        <ListItem button component={Link} to='/skills'>
            <ListItemIcon>
                <Create />
            </ListItemIcon>
            <ListItemText primary="Skills" />
        </ListItem>
    </div>
);