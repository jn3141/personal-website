import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from 'material-ui/styles';
import { blue } from 'material-ui/colors';
import AppBar from 'material-ui/AppBar';
import MenuIcon from 'material-ui-icons/Menu';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';

const styles = theme => ({
    root: {
        width: 'calc(100% + 8 * 2)',
        marginTop: -8,
        marginLeft: -8,
        marginRight: -8,
    },
    flex: {
        flex: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
});

function TopNav(props) {
    const classes = props.classes;

    return (
        <div className={classes.root}>
            <AppBar position="static" style={{backgroundColor: blue[600]}}>
                <Toolbar>
                    <IconButton className={classes.menuButton} color="contrast" aria-label="Menu" onClick={props.menuOnClick}>
                        <MenuIcon />
                    </IconButton>
                    <Typography type="title" color="inherit" className={classes.flex}>
                        Personal Portfolio
                    </Typography>
                </Toolbar>
            </AppBar>
        </div>
    );
}

TopNav.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TopNav);