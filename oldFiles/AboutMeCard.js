import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from 'material-ui/styles';

import AccountCircle from 'material-ui-icons/AccountCircle';
import Card, { CardContent } from 'material-ui/Card';
import Typography from 'material-ui/Typography';

const styles = theme => ({
    card: {
        margin: '16px 8px 16px 8px',
    },
    headingText: {
        color: theme.palette.primary[700],
        margin: '8px 0px 8px 0px',
    },
    highlightText: {
        color: theme.palette.primary[700],
        'font-weight': 'bold',
    },
    icon: {
        width: '30px',
        height:'100%',
        'vertical-align': 'top',
    },
    row: {
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
    },
});

function AboutMeCard(props) {
    const { classes } = props;
    
    return (
        <div>
            <Card className={classes.card}>
                <CardContent>
                    <Typography type="headline" className={classes.headingText} >
                        <AccountCircle className={classes.icon}/> A Brief Intro
                    </Typography>
                    <Typography type="subheading" component="p">
                        My name is <font className={classes.highlightText}>Justin Ng</font>, a current student at the <font className={classes.highlightText}>University of New South Wales</font> studying <font className={classes.highlightText}>Computer Science/Law</font>.<br/>
                        I strive to be an <font className={classes.highlightText}>innovator who brings new technology to new fields</font>, and have a constant drive to develop solutions are painless to use.<br/>
                        Please feel free to browse my website and explore my portfolio and qualifications, to see what experience I have across various technologies.<br/>
                        I am always looking for new projects to build, and the next opportunity to learn more, so please get in touch with me if you'd like to build something together!
                    </Typography>
                </CardContent>
            </Card>
        </div>
    );
}

AboutMeCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AboutMeCard);