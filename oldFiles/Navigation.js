import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from 'material-ui/styles';
import Drawer from 'material-ui/Drawer';
import List from 'material-ui/List';

import { NavigationList } from './NavigationList';
import TopNav from './TopNav';

const styles = {
    list: {
        width: 250,
    },
};

class Navigation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
        }
    }

    toggleDrawer(isOpen, mode) {
        this.setState({
            isOpen: isOpen,
        });
    }

    render() {
        const classes = this.props.classes;

        const sideList = (
            <div className={classes.list}>
                <List>{NavigationList}</List>
            </div>
        );

        return (
            <div>
                <TopNav menuOnClick={() => this.toggleDrawer(true)}/>
                <Drawer open={this.state.isOpen} onRequestClose={() => this.toggleDrawer(false)}>
                    <div
                        tabIndex={0}
                        role="button"
                        onClick={() => this.toggleDrawer(false)}
                        onKeyDown={() => this.toggleDrawer(false)}>
                    {sideList}
                    </div>
                </Drawer>
            </div>
        );
    }
}

Navigation.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Navigation);