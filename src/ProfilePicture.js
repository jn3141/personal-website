import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from 'material-ui/styles';
import Avatar from 'material-ui/Avatar';

import ProfilePictureSource from './images/saitama_by_ry_spirit-d9n2269.jpg';

const styles = {
    row: {
        display: 'flex',
        justifyContent: 'center',
        padding: '40px 40px 20px 40px',
    },
    bigAvatar: {
        width: '240px',
        height: '240px',
    }
};

function ProfilePicture(props) {
    const { classes } = props;

    return (
        <div className={classes.row}>
            <Avatar alt="Justin Ng" src={ProfilePictureSource} className={classes.bigAvatar} />
        </div>
    );
}

ProfilePicture.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProfilePicture);