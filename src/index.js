import React from 'react';
import ReactDOM from 'react-dom';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import { indigo, yellow } from 'material-ui/colors';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import Grid from './Grid'
import TopCard from './TopCard';
import 'typeface-roboto';

const theme = createMuiTheme({
    palette: {
        primary: indigo,
        secondary: yellow,
        "common": {
            "black": "#000",
            "white": "#fff",
        },
    },
    section: {
        marginTop: '16px',
    },
});

function App(props) {
    return (
        <MuiThemeProvider theme={theme}>
            <TopCard />
            <main>
                <Switch>
                    <Route exact path='/' component={Grid} />
                    <Route path='/projects'/>
                </Switch>
            </main>
        </MuiThemeProvider>
    );
}

ReactDOM.render(
    (<BrowserRouter>
        <App />
    </BrowserRouter>),
    document.getElementById('app')
);