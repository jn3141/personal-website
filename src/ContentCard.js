import React from 'react';
import PropTypes from 'prop-types';

import Card, { CardContent } from 'material-ui/Card';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';

import AccountCircle from 'material-ui-icons/AccountCircle'
// import Build from 'material-ui-icons/Build';
import Create from 'material-ui-icons/Create';
import School from 'material-ui-icons/School';
// import StarIcon from 'material-ui-icons/Star';
import Work from 'material-ui-icons/Work';

import Education from './CardContents/Education';
import Skills from './CardContents/Skills';
import WorkExperience from './CardContents/WorkExperience';
import data from './Data.json';

const styles = theme => ({
    card: {
        margin: '16px 8px 16px 8px',
    },
    description: {
        'white-space': 'pre-line', 
    },
    headingText: {
        color: theme.palette.primary[700],
        margin: '8px 0px 8px 0px',
    },
    highlightText: {
        color: theme.palette.primary[700],
        'font-weight': 'bold',
    },
    icon: {
        width: '30px',
        height:'100%',
        'vertical-align': 'top',
    },
    section: {
        marginTop: '16px',
    },
});

class ContentCard extends React.Component {
    createABriefIntro = () => {
        const classes = this.props.classes;
        return (
            <div className={classes.section}>
                <Typography type="subheading" component="p">
                    My name is <font className={classes.highlightText}>Justin Ng</font>, a current student at the <font className={classes.highlightText}>University of New South Wales</font> studying <font className={classes.highlightText}>Computer Science/Law</font>.<br/>
                    I strive to be an <font className={classes.highlightText}>innovator who brings new technology to new fields</font>, and have a constant drive to develop solutions that are painless to use.<br/>
                    Please feel free to browse my website and explore my portfolio and qualifications, to see what experience I have across various technologies.<br/>
                    I am always looking for new projects to build, and the next opportunity to learn more, so please get in touch with me if you'd like to build something together!
                </Typography>
            </div>
        )
    }

    createEducationSection = (educationObject) => {
        return <Education schoolName={educationObject.schoolName} degree={educationObject.degree} dates={educationObject.dates} description={educationObject.description} />
    }

    createSkillsSection = (skillsObject) => {
        return <Skills type={skillsObject.type} list={skillsObject.list}/>
    }

    createWorkExperienceSection = (workExperienceObject) => {
        return <WorkExperience title={workExperienceObject.title} companyName={workExperienceObject.companyName} datesEmployed={workExperienceObject.datesEmployed} description={workExperienceObject.description} />
    }

    render() {
        const classes = this.props.classes;
        const type = this.props.type;

        var icon = "";
        var heading = "";
        var content = "";
        
        if (type === 'aboutMe') {
            icon = <AccountCircle className={classes.icon}/>;
            heading = 'A Brief Intro';
            content = this.createABriefIntro();
        } else if (type === 'education') {
            icon = <School className={classes.icon}/>;
            heading = 'Education';
            content = data.education.map(this.createEducationSection);
        } else if (type === 'skills') {
            icon = <Create className={classes.icon}/>;
            heading = 'Skills';
            content = data.skills.map(this.createSkillsSection);
        } else if (type === 'workExperience') {
            icon = <Work className={classes.icon}/>;
            heading = 'Work Experience';
            content = data.workExperience.map(this.createWorkExperienceSection);
        }

        return (
            <div>
                <Card className={classes.card}>
                    <CardContent>
                        <Typography type="headline" className={classes.headingText} >
                            {icon} {heading}
                        </Typography>
                        {content}
                    </CardContent>
                </Card>
            </div>
        );
    }
}

ContentCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ContentCard);