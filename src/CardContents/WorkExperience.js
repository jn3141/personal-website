import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from 'material-ui/styles';

import Typography from 'material-ui/Typography';

const styles = theme => ({
    description: {
        'white-space': 'pre-line', 
    },
    titleColour: {
        color: theme.palette.primary[700],
    },
    section: {
        marginTop: '16px',
    },
});

function WorkExperience(props) {
    const classes = props.classes;
    const title = props.title;
    const companyName = props.companyName;
    const datesEmployed = props.datesEmployed;
    const description = props.description;

    return (
        <div className={classes.section}>
            <Typography type="caption">
                {datesEmployed}
            </Typography>
            <Typography type="title" className={classes.titleColour}>
                {title}
            </Typography>
            <Typography type="subheading">
                {companyName}
            </Typography>
            <Typography type="body1">
                <font className={classes.description}>{description}</font>
            </Typography>
        </div>
    );
}

WorkExperience.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(WorkExperience);