import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from 'material-ui/styles';

import Typography from 'material-ui/Typography';

const styles = theme => ({
    description: {
        'white-space': 'pre-line', 
    },
    titleColour: {
        color: theme.palette.primary[700],
    },
    section: {
        marginTop: '16px',
    },
});

function Education(props) {
    const classes = props.classes;
    const degree = props.degree;
    const schoolName = props.schoolName;
    const dates = props.dates;
    const description = props.description;

    return (
        <div className={classes.section}>
            <Typography type="caption">
                {dates}
            </Typography>
            <Typography type="title" className={classes.titleColour}>
                {schoolName}
            </Typography>
            <Typography type="subheading">
                {degree}
            </Typography>
            <Typography type="body1">
                <font className={classes.description}>{description}</font>
            </Typography>
        </div>
    );
}

Education.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Education);