import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from 'material-ui/styles';
import Chip from 'material-ui/Chip';
import Typography from 'material-ui/Typography';

const styles = theme => ({
    chip: {
        background: theme.palette.secondary[500],
        color: theme.palette.common.black,
        'font-style': 'oblique',
        margin: theme.spacing.unit,
    },
    description: {
        'white-space': 'pre-line', 
    },
    row: {
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
    },
    titleColour: {
        color: theme.palette.primary[700],
    },
    section: {
        marginTop: '16px',
    },
});

class Skills extends React.Component {
    createSkillChips = (label) => {
        return <Chip label={label} className={this.props.classes.chip} />
    }

    render() {
        const classes = this.props.classes;
        const type = this.props.type;
        const list = this.props.list;
        
        return (
            <div className={classes.section}>
                <Typography type="title" className={classes.titleColour}>
                    {type}
                </Typography>
                <Typography type="body1" className={classes.row}>
                    {list.map(this.createSkillChips)}
                </Typography>
            </div>
        );
    }
}

Skills.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Skills);