import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from 'material-ui/styles';
import Card, { CardContent } from 'material-ui/Card';
import Chip from 'material-ui/Chip';
import ProfilePicture from './ProfilePicture';
import Typography from 'material-ui/Typography';
import data from './Data.json';

const styles = theme => ({
    card: {
        width: 'calc(100% - 8px * 2)',
        margin: '16px 8px 8px 8px',
        background: theme.palette.primary[700],
    },
    chip: {
        background: theme.palette.secondary[500],
        color: theme.palette.common.black,
        'font-style': 'oblique',
        margin: theme.spacing.unit,
    },
    headingText: {
        color: theme.palette.common.white,
        marginTop: '20px',
    },
    row: {
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
    },
});

class TopCard extends React.Component {
    createTags = (tag) => {
        return <Chip label={tag} className={this.props.classes.chip} /> 
    }

    render() {
        const classes = this.props.classes;

        return (
            <div>
                <Card className={classes.card}>
                    <CardContent>
                        <ProfilePicture />
                        <Typography type="display1" gutterBottom align="center" className={classes.headingText}>
                            Justin Ng
                        </Typography>
                        <div className={classes.row}>
                            {data.tags.map(this.createTags)}
                        </div>
                    </CardContent>
                </Card>
            </div>
        );
    }
}

TopCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TopCard);