# My Personal Website

This is my personal website, which is created using ReactJS (and of course JavaScript), as well as Google Material UI. It was bootstrapped using Create-React-App.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing

Clone this repo:

```
git clone https://bitbucket.org/jn3141/personal-website.git
cd personal-website
```

Install the npm dependencies, and run it:

```
npm install
npm start
```